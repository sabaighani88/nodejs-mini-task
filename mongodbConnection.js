///////////////////////////////

module.exports = class MongoDBConnection {
  constructor() {}

  connection() {
    const e = require("express");
    const { number } = require("joi");
    const mongoose = require("mongoose");
    var Item = require("./itemModel");
    mongoose
      .connect("mongodb://localhost/webProject", {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      })
      .then(() => {
        console.log("connected to mongodb..");
      })
      .catch((err) => console.log("error to connect db", err));
    return Item;
  }

  async createItem(newItem, Items) {
    const item = new Items({
      itemName: newItem.itemName,
      itemCount: newItem.itemCount,
    });
    const result = await item.save();
    //console.log(result);
    return result;
  }
 

  async getItems(connection) {
    const item = await connection.find().sort({ date: -1 });
    return item;
  }
  async getItem(Count,Items) {
    const item = await Items.find({ itemCount: Count });
    console.log(item);
    return item
  }
  
  async updateItem(item, Items) {
    const selectedItem = await Items.findById(item.id);
    if (!selectedItem) {
      return "not exists";
    } else {
      selectedItem.itemName = item.itemName;
      selectedItem.itemCount = item.itemCount;
      const result = await selectedItem.save();
      console.log(result);
      return result;
    }
  }

  async deleteItem(item,Items){
      const result=await Items.findByIdAndRemove(item.id)
      if(result){
          console.log(result)
      }
      return result
 
    }
};
