const mongoose=require('mongoose');

const itemSchema= new mongoose.Schema({
    itemName : String,
    itemCount : Number,
    date:{type : Date,default:Date.now}
})

module.exports = mongoose.model('Item',itemSchema)