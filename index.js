const express = require("express")
// const logger = require('morgan')
const logger = require('./logger')
const MongoDBConnection = require("./mongodbConnection");
const Joi = require("joi");
const app = express();

app.use(express.json());

app.use(express.urlencoded({ extended: true }))
app.use(logger);
const port = process.env.PORT || 8000;
app.listen(port, () => {
  console.log(`listening on ${port}...`);
});
// app.use(MongoDBConnection)
app.post("/addItem", (req, res) => {

  
  const addItem = async () => {
    // const result=await ValidateItem(req.body)
    // if (result.error){
    //    res.status(400).send(result.error.details[0].message);
    // }
   
    const db = new MongoDBConnection();
    const connection = await db.connection();
    const item = await db.createItem(req.body, connection);
    if(item){
      res.json(item).status(200)
    }
  };
 addItem();
});
app.get("/getItems", (req, res) => {
  getitems = async () => {
    const db = new MongoDBConnection();
    const connection = await db.connection();
    const items = await db.getItems(connection);

    res.json(items);
    if (items) {
      //  res.status(200).send(items)
    } else {
      //  res.status(400).send("get item error")
    }
  };
  getitems();
});



app.get("/getItem:count",(req,res)=>{
  getItem=async()=>{
  const db = new MongoDBConnection();
  const connection = await db.connection();
  const items = await db.getItem(req.params.count,connection);
  res.json(items);
  }
  getItem()
})
app.post("/deleteItem", (req, res) => {
  console.log("deleteItem");
  console.log(req.body)
  const deleteItem = async () => {

    const db = new MongoDBConnection();
    const ItemSchema = await db.connection();
    
    const result = await db.deleteItem(req.body, ItemSchema);
    if(result){
      res.json(result).status(200)
    }
  };
  deleteItem();
});
app.put("/editItem", (req, res) => {
  console.log("edit Item");
  const updateItem = async () => {

    const db = new MongoDBConnection();
    const ItemSchema = await db.connection();
    
    const result = await db.updateItem(req.body, ItemSchema);
    if(result){
      res.json(result).status(200)
    }
  };
  updateItem();
});




function ValidateItem(item) {
  console.log(item);

  const inputschema = {
    //.regex('/[a-zA-Z]{1}{20}/')

    itemName: Joi.string().min(1).max(20).required(),
    itemCount: Joi.number().required(),
  };
  const result = Joi.valid(item, inputschema);
  //console.log(result.error)
  return result;
}
